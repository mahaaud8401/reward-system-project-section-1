<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<?php
	include "nav.php";
		?>

		<div class="container box">
			<div class="alert alert-success">
				<strong>Welcom to Reward System</strong> , please select your task
			</div>
			<ol class="breadcrumb">
				<li>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Library</a>
				</li>
				<li class="active">
					Data
				</li>
			</ol>
			<div class="row" style="padding: 30px;">
				<center><div class="col-xs-6 col-sm-4">
					<a href="#"><img src="img/login.png" class="img-responsive" />
					<h1> Login </h1>
					</a>
				</div>
				<div class="col-xs-6 col-sm-4">
					<a href="#"><img src="img/rewardStudent.png" class="img-responsive" />
					<h1> Leader Board </h1>
					<small> rank by student  </small>
					</a>
				</div>
				<!-- Optional: clear the XS cols if their content doesn't match in height -->
				<div class="clearfix visible-xs"></div>
				<div class="col-xs-6 col-sm-4">
					<a href="#"><img src="img/homeReward.png" class="img-responsive" />
					<h1> Leader Board  </h1>
					<small> rank by bann  </small>
					</a>
				</div>
			</div></center>
		</div>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
	</body>
</html>