<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
	    <link href="css/bootstrap.css" rel="stylesheet">
	    <link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

<?php include "nav.php"; ?>

		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<p>
							<center><img src="img/person.jpg" style="width: 80%;">
							</center>
						</p>

					</div>
				</div>
				
				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						<h1> Rules Management
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Edit <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-normal" role="menu">
								<li>
									<a href="#">avartar</a>
								</li>
							</ul>
						</div></h1>
											
						<hr />
						<ul class="nav nav-tabs" id="rule_option">
							  <li class="active"><a href="#add_rule" data-toggle="tab">Add Rule</a></li>
							  <li><a href="#edit_rule" data-toggle="tab">Edit Rule</a></li>
							  <li><a href="#delete_rule" data-toggle="tab">Delete Rule</a></li>
							  <li><a href="#show_rule" data-toggle="tab">Show Rule</a></li>
						</ul>
						<br/>
						
						<div class="tab-content">
							<div class="tab-pane active" id="add_rule">
								<table class="table table-bordered align_center" id="ruleTable">
									<tr>
										<td colspan="2" class="td-panel">
											<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-plus"> </span> Add new rule</h4>
										</td>
									</tr>
									<tr>
										<td style="width: 15%;">Rule ID: </td>
										<td><input type="number" name="rule_id"  class="form-control" ></td>
									</tr>
									<tr>
										<td style="width: 15%;">Rule Name:</td>
										<td><input type="text" name="rule_name" class="form-control"></td>
									</tr>
									<tr>
										<td style="width: 15%;">Point:</td>
										<td><input type="number" name="point" class="form-control"></td>
									</tr>
									<tr>
										<td style="width: 15%;">Start Date:</td>
										<td><input type="date" name="start_date" class="form-control"></td>
									</tr>
									<tr>
										<td style="width: 15%;">End Date:</td>
										<td><input type="date" name="end_date" class="form-control"></td>
									</tr>
									<tr>
										<td style="width: 15%;">Email:</td>
										<td><input type="text" name="end_date" class="form-control"></td>
									</tr>
								</table>
								<center><button type="submit" class="btn btn-lg btn-primary">Add</button></center>
							</div>
							<div class="tab-pane" id="edit_rule">
								<table class="table table-bordered align_center" id="ruleTable">
									<tr>
										<td colspan="2" class="td-panel">
											<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-pencil"> </span> Edit Rule</h4>
										</td>
									</tr>
									<tr>
										<td style="width: 15%;">Rule ID: </td>
										<td>
											<p>Rule ID</p>
											<input type="text" name="rule_name" class="form-control" id="ruleFilled" placeholder="edit rule ID">
										</td>
									</tr>
									<tr>
										<td style="width: 15%;">Rule Name:</td>
										<td>
											<p>Rule Name</p>
											<input type="number" name="point" class="form-control" id="ruleFilled" placeholder="edit rule name">
										</td>
									</tr>
									<tr>
										<td style="width: 15%;">Point:</td>
										<td>
											<p>Point</p>
											<input type="date" name="start_date" class="form-control" id="ruleFilled" placeholder="edit point">
										</td>
									</tr>
									<tr>
										<td style="width: 15%;">Start Date:</td>
										<td><input type="date" name="start_date" class="form-control"></td>
									</tr>
									<tr>
										<td style="width: 15%;">End Date:</td>
										<td><input type="date" name="end_date" class="form-control"></td>
									</tr>
								</table>
								<center><button type="submit" class="btn btn-lg btn-primary">Edit</button></center>
							</div>
							<div class="tab-pane" id="delete_rule">
								<div class="panel panel-default">
								  <!-- Default panel contents -->
								  <div class="panel-heading"><h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-remove"> </span> Delete rule</h4></div>
								
								  <!-- Table -->
							      <table class="table">
							        <thead>
							          <tr>
							            <th>Rule ID</th>
							            <th>Rule Name</th>
							            <th>Point</th>
							            <th>Duration</th>
							          </tr>
							        </thead>
							        <tbody>
							          <tr>
							            <td>1</td>
							            <td>Mark</td>
							            <td>Otto</td>
							            <td>@mdo</td>
							             <td><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-remove"> </span></button></td>
							          </tr>
							          <tr>
							            <td>2</td>
							            <td>Jacob</td>
							            <td>Thornton</td>
							            <td>@fat</td>
							             <td><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-remove"> </span></button></td>
							          </tr>
							          <tr>
							            <td>3</td>
							            <td>Larry</td>
							            <td>the Bird</td>
							            <td>@twitter</td>
							             <td><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-remove"> </span></button></td>
							          </tr>
						
							        </tbody>
							      </table>
								</div>
							</div>
							<div class="tab-pane" id="show_rule">
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Show Rules</h4>
									</div>
		
									<!-- Table -->
									<table class="table">
										<thead>
											<tr>
												<th>Rule ID</th>
												<th>Rule Name</th>
												<th>Point</th>
												<th>Duration</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>Larry</td>
												<td>the Bird</td>
												<td>@twitter</td>
												<td><!-- Large modal -->
												<button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">
													<span class="glyphicon glyphicon-pencil"></span>
												</button>
												<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																	&times;
																</button>
																<h4 class="modal-title" id="myLargeModalLabel"><h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-pencil"> </span> Rules Management</h4></h4>
															</div>
															<div class="modal-body">
																<div class="container">
																	<table class="table table-bordered" id="ruleTable">
																		<tr>
																			<td colspan="2" class="td-panel"><h4>&nbsp;&nbsp; Edit Rule</h4></td>
																		</tr>
																		<tr>
																			<td style="width: 15%;">Rule ID: </td>
																			<td>
																			<p>
																				Rule ID
																			</p>
																			<input type="text" name="rule_name" class="form-control" id="ruleFilled" placeholder="edit rule ID">
																			</td>
																		</tr>
																		<tr>
																			<td style="width: 15%;">Rule Name:</td>
																			<td>
																			<p>
																				Rule Name
																			</p>
																			<input type="number" name="point" class="form-control" id="ruleFilled" placeholder="edit rule name">
																			</td>
																		</tr>
																		<tr>
																			<td style="width: 15%;">Point:</td>
																			<td>
																			<p>
																				Point
																			</p>
																			<input type="date" name="start_date" class="form-control" id="ruleFilled" placeholder="edit point">
																			</td>
																		</tr>
																		<tr>
																			<td style="width: 15%;">Duration:</td>
																			<td>
																			<input type="date" name="start_date" class="form-control">
																			</td>
																		</tr>
																	</table>
																	<button type="submit" class="btn btn-lg btn-primary">
																		Edit
																	</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
		
										</tbody>
									</table>
		
								</div>
							</div>
							</div>
						</div>
					</div>
					</div>

				</div>

			</div>
		</div>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
		<script>
			$('#rule_option a:first').tab('show');
		</script>
	</body>
</html>