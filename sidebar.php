<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reward System</title>

    <!-- Bootstrap -->
    <link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<?php include "nav.php"; ?>
  	
  	<div class="container">
  		<div class="row">
	  		<div class="col-sm-12 col-md-9 col-lg-9">
	  			<div class="alert alert-warning"><strong>Note:</strong> This is a page with sidebar. If you want to learn more about how to use Twitter Bootstrap's components, please visit <a href="http://getbootstrap.com/" target="_blank">http://getbootstrap.com/</a></div>
	  			<div class="box">
	  				<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="#">Library</a></li>
						<li class="active">Data</li>
					</ol>
			  		<h1>Page Heading <small>Subheading here.</small></h1>
			  		<hr />
			  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			  	</div>
	  		</div>
	  		<div class="col-sm-12 col-md-3 col-lg-3">
	  			<div class="box">
	  				<p>
	  					<p class="lead">Sidebar</p>
	  					<ul class="nav nav-pills nav-stacked">
	  						<li class="active"><a href="#">Submenu 1 <span class="badge pull-right">23</span></a></li>
	  						<li><a href="#">Submenu 2</a></li>
	  						<li><a href="#">Submenu 3</a></li>
	  						<li><a href="#">Submenu 4 <span class="badge pull-right">7</span></a></li>
	  					</ul>
	  				</p>
	  			</div>
	  		</div>
  		</div>
  	</div>
  	
  	<div class="container box" id="footer">
  		&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved. 
  	</div>
  	<a href="#" id="scroll_top">Scroll</a>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scroll.js"></script>
  </body>
</html>