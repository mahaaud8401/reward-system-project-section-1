<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

<?php include "nav.php"; ?>		

		<div class="container box">
			<h1><span class="glyphicon glyphicon-user"> </span> Login</h1>
			<hr />
			
			<div id="login_box">
				<div class="alert alert-warning">
					<strong>Warning!</strong> please fill your username and password
				</div>
				<div class="alert alert-danger">
					<strong>Error!</strong> Incorrect username or password
				</div>
				<form class="form-horizontal" id="login_form" role="form">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="inputUsername" placeholder="Username">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="inputPassword" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox">
									Remember me </label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-lg btn-primary">
								Sign in
							</button>
						</div>
					</div>
				</form>
			</div>
			
		</div>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
	</body>
</html>