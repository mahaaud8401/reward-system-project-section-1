<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<?php
		include "nav.php";
		?>

		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<p>
							<center><img src="img/person.jpg" style="width: 80%;">
							</center>
						</p>

					</div>
				</div>

				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						<h1><span class="glyphicon glyphicon-user"> </span> Instructor / Pisito PoPo <!-- Single button -->
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Edit <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-normal" role="menu">
								<li>
									<a href="#">avartar</a>
								</li>
							</ul>
						</div></h1>

						<hr />
						<ul class="nav nav-tabs" id="rule_option">
							<li class="active">
								<a href="#profile" data-toggle="tab">Profile</a>
							</li>
							<li>
								<a href="#search_student" data-toggle="tab">Search Student</a>
							</li>
							<li>
								<a href="#show_rule" data-toggle="tab">Show Rule</a>
							</li>
						</ul>
						<br/>

						<div class="tab-content">
							<div class="tab-pane active" id="profile">
								<table class="table table-bordered">
									<tr>
										<td colspan="2" class="td-panel"><h4>&nbsp;&nbsp;Profile</h4></td>
									</tr>
									<tr>
										<td style="width: 15%;">Instructor ID:</td>
										<td>asdasdsadfasdfsadasd</td>
									</tr>
									<tr>
										<td style="width: 15%;">Email:</td>
										<td></td>
									</tr>
									<tr>
										<td style="width: 15%;">Tel:</td>
										<td></td>
									</tr>
								</table>
							</div>
							<div class="tab-pane" id="search_student">
								</br>
								<div class="input-group">
									<span class="input-group-addon">student search</span>
									<input type="text" class="form-control" >
									<div class="input-group-btn">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											search <span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="#">Action</a>
											</li>
											<li>
												<a href="#">Another action</a>
											</li>
											<li>
												<a href="#">Something else here</a>
											</li>
											<li class="divider"></li>
											<li>
												<a href="#">Separated link</a>
											</li>
										</ul>
									</div><!-- /btn-group -->
								</div><!-- /input-group -->
								</br>
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Search Result</h4>
									</div>

									<!-- Table -->
									<table class="table">
										<thead>
											<tr>
												<th>Student ID</th>
												<th>Student Name</th>
												<th>Year</th>
												<th>Baan</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>5488022</td>
												<td>Jenneth Chaovisutikul</td>
												<td>3</td>
												<td>3</td>
												<td>
												<center>
													<!-- Large modal -->
													<button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">
														<span class="glyphicon glyphicon-star"></span> give reward
													</button>
												</center>
												<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																	&times;
																</button>
																<h4 class="modal-title" id="myLargeModalLabel"><h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-star"></span> Give Reward</h4>
															</div>
															<div class="modal-body">

																<div class="alert alert-info">
																	Click student's name to see full information
																</div>
																<div class="panel panel-default">
																	<!-- Default panel contents -->
																	<div class="panel-heading">
																		<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <h3>Jenneth Chaovisutikul</h3> </a>
																	</div>
																	<!-- collapse-->
																	<div id="collapseOne" class="panel-collapse collapse">
																		<table class="table table-bordered">
																			<tr>
																				<td colspan="2" class="td-panel"><h4>&nbsp;&nbsp;Profile</h4></td>
																			</tr>
																			<tr>
																				<td style="width: 15%;">Student ID:</td>
																				<td>asdasdsadfasdfsadasd</td>
																			</tr>
																			<tr>
																				<td style="width: 15%;">Year:</td>
																				<td>asdasdasdsadsadsad</td>
																			</tr>
																			<tr>
																				<td style="width: 15%;">Baan:</td>
																				<td>sadasdasdsadasd</td>
																			</tr>
																			<tr>
																				<td style="width: 15%;">Track:</td>
																				<td></td>
																			</tr>
																			<tr>
																				<td style="width: 15%;">Email:</td>
																				<td></td>
																			</tr>
																			<tr>
																				<td style="width: 15%;">Tel:</td>
																				<td></td>
																			</tr>
																		</table>
																	</div><!-- end collapse-->
																	<!-- reward -->
																	<div class="panel-heading">
																		<h4>Reward</h4>
																	</div>
																	<div class="row" style="padding: 20px;">
																		</br>
																		<div class="col-sm-12 col-md-6 col-lg-6">

																			<div class="panel panel-primary" style="text-align: center;">
																				<div class="panel-heading">
																					<h4>Student's score</h4>
																				</div>
																				<div class="panel-body">
																					100 points
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-12 col-md-6 col-lg-6">
																			<div class="panel panel-danger" style="text-align: center;">
																				<div class="panel-heading">
																					<h4>Baan's score</h4>
																				</div>
																				<div class="panel-body">
																					100 points
																				</div>
																			</div>
																		</div>
																	</div>
																	<!-- end reward-->
																	<!-- rule -->
																	<div class="panel-heading">
																		<h4>Rules &nbsp;&nbsp;&nbsp;&nbsp;
																		<div class="btn-group">
																			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
																				Sort by <span class="caret"></span>
																			</button>
																			<ul class="dropdown-menu" role="menu">
																				<li>
																					<a href="#">Rule ID</a>
																				</li>
																				<li>
																					<a href="#">Rule Name</a>
																				</li>
																				<li>
																					<a href="#">Point</a>
																				</li>
																				<li>
																					<a href="#">Duration</a>
																				</li>
																			</ul>
																		</div></h4>
																	</div>
																	<!--end rule -->
																	<br>
																	<div class="panel-body">
																		<div class="input-group">
																			<span class="input-group-addon">rule search</span>
																			<input type="text" class="form-control" >
																			<div class="input-group-btn">
																				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
																					search <span class="caret"></span>
																				</button>
																				<ul class="dropdown-menu pull-right">
																					<li>
																						<a href="#">Action</a>
																					</li>
																					<li>
																						<a href="#">Another action</a>
																					</li>
																					<li>
																						<a href="#">Something else here</a>
																					</li>
																					<li class="divider"></li>
																					<li>
																						<a href="#">Separated link</a>
																					</li>
																				</ul>
																			</div><!-- /btn-group -->
																		</div><!-- /input-group -->
																		<br>

																		<!-- Table -->
																		<table class="table">
																			<thead>
																				<tr>
																					<th>Rule ID</th>
																					<th>Rule Name</th>
																					<th>Point</th>
																					<th>Duration</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>1</td>
																					<td>Larry</td>
																					<td>the Bird</td>
																					<td>@twitter</td>

																				</tr>

																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div></td>

											</tr>

										</tbody>
									</table>

								</div>
							</div>

							<div class="tab-pane" id="show_rule">
								</br>
								<div class="input-group">
									<span class="input-group-addon">rule search</span>
									<input type="text" class="form-control" >
									<div class="input-group-btn">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											search <span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="#">Action</a>
											</li>
											<li>
												<a href="#">Another action</a>
											</li>
											<li>
												<a href="#">Something else here</a>
											</li>
											<li class="divider"></li>
											<li>
												<a href="#">Separated link</a>
											</li>
										</ul>
									</div><!-- /btn-group -->
								</div><!-- /input-group -->
								</br>
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Show Rules</h4>
									</div>

									<!-- Table -->
									<table class="table">
										<thead>
											<tr>
												<th>Rule ID</th>
												<th>Rule Name</th>
												<th>Point</th>
												<th>Duration</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>Larry</td>
												<td>the Bird</td>
												<td>@twitter</td>
												
											</tr>

										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		</div>
		</div>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
		<script>
$('#rule_option a:first''').tab('show');
		</script>
	</body>
</html>