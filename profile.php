<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

<?php include "nav.php"; ?>

		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<p>
							<center><img src="img/person.jpg" style="width: 80%;">
							</center>
						</p>

					</div>
				</div>
				
				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						 <h1><span class="glyphicon glyphicon-user"> </span> Student / Jenneth Chaovisutikul <!-- Single button -->
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Edit <span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-normal" role="menu">
								<li>
									<a href="#">profile</a>
								</li>
								
								<li>
									<a href="#">avartar</a>
								</li>
							
							</ul>
						</div></h1>
						<hr />
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-6">
								<div class="panel panel-primary" style="text-align: center;">
									<div class="panel-heading">
										<h4>Student's Score</h4>
									</div>
									<div class="panel-body">
										100 points
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-6 col-lg-6">
								<div class="panel panel-danger" style="text-align: center;">
									<div class="panel-heading">
										<h4>Baan's Score</h4>
									</div>
									<div class="panel-body">
										100 points
									</div>
								</div>
							</div>
						</div>
						
						<table class="table table-bordered">
							<tr>
								<td colspan="2" class="td-panel"><h4>&nbsp;&nbsp;Profile</h4></td>
							</tr>
							<tr>
								<td style="width: 15%;">Student ID:</td>
								<td>asdasdsadfasdfsadasd</td>
							</tr>
							<tr>
								<td style="width: 15%;">Year:</td>
								<td>asdasdasdsadsadsad</td>
							</tr>
							<tr>
								<td style="width: 15%;">Baan:</td>
								<td>sadasdasdsadasd</td>
							</tr>
							<tr>
								<td style="width: 15%;">Track:</td>
								<td></td>
							</tr>
							<tr>
								<td style="width: 15%;">Email:</td>
								<td></td>
							</tr>
							<tr>
								<td style="width: 15%;">Tel:</td>
								<td></td>
							</tr>
						</table>
					</div>

				</div>

			</div>
		</div>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
	</body>
</html>