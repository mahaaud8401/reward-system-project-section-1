<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
			.col-centered	{
			    float: none;
			    margin: 0 auto;
			    clear: left;
			}
		</style>
	</head>
	<body>

		<?php
		include "nav.php";
		?>

		<div class="container">
			
			<div class="row">
				
				
				
				
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="box">
						<div class="row">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
							</div>
							<div class="col-lg-10">
								<h1 style="font-family: 'gnuolane'">
	 <strong>Nutchanon Phongoen</strong></h1>
	 							<h4><span class="label label-primary">5488115</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 2</span></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<center>
			 						<h1><small>Score:</small><br /><span style="font-size: 3em; color: #f4c000; font-weight: bold;">380</span></h1>
			 					</center>
							</div>
							<div class="col-lg-4">
								<center><img src="img/gold.png" class="img-responsive" /></center>
							</div>
							<div class="col-lg-2"></div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-2">&nbsp;</div>
				
			</div>
			
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
			 							<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #abb4ac; font-weight: bold;">200</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/silver.png" class="img-responsive" /></center>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Parit Waitayakomol</strong></h2>
			 							<h4><span class="label label-primary">5488213</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 2</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #cd796a; font-weight: bold;">100</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/bronze.png" class="img-responsive" /></center>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2">&nbsp;</div>
			</div>
		</div></br>
		<center>	<img src="img/ribbon3.png" /></center>
		</br></br>
		<div class="container visible-lg">
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">70</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/4.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">60</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/5.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">50</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/6.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">40</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/7.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">30</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/8.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">20</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/9.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12 col-lg-11 col-centered">
						<div class="box">
							<div class="row">
								<div class="col-sm-2 col-md-2 col-lg-2">
									<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
								</div>
								<div class="col-sm-5 col-md-5 col-lg-5">
									<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
					 				<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
								</div>
								<div class="col-sm-3 col-md-3 col-lg-3">
									<h1><small>Score:</small><br /><span style="font-size: 1.3em; color: #333; font-weight: bold;">10</span></h1>
								</div>
								<div class="col-sm-2 col-md-2 col-lg-2">
									<center><img src="img/10.png" class="img-responsive" /></center>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
		
		
		
		<div class="container hidden-lg">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
			 							<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
									</div>
								</div>
								<br />
								
								<div class="row">
								mmm
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">70</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/4.png" class="img-responsive" /></center>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Parit Waitayakomol</strong></h2>
			 							<h4><span class="label label-primary">5488213</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 2</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">60</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/5.png" class="img-responsive" /></center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2">&nbsp;</div>
			</div>
		</div>
		
		<div class="container hidden-lg">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
			 							<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">50</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/6.png" class="img-responsive" /></center>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Parit Waitayakomol</strong></h2>
			 							<h4><span class="label label-primary">5488213</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 2</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">40</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/7.png" class="img-responsive" /></center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container hidden-lg">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
			 							<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">30</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/8.png" class="img-responsive" /></center>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Parit Waitayakomol</strong></h2>
			 							<h4><span class="label label-primary">5488213</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 2</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">20</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/9.png" class="img-responsive" /></center>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container hidden-lg">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="box">
								<div class="row">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
										<img src="img/person.jpg" class="img-responsive img-circle" style="vertical-align: middle;" id="leaderImg" />
									</div>
									<div class="col-lg-9">
										<h2 style="font-family: 'gnuolane'"><strong>Pana Amphaisakul</strong></h2>
			 							<h4><span class="label label-primary">5488003</span> <span class="label label-success">3<sup>rd</sup> Year</span> <span class="label label-info">Bann 1</span></h4>
									</div>
								</div>
								<br />
								<div class="row">
									<div class="col-lg-6">
										<center>
					 						<h1><small>Score:</small><br /><span style="font-size: 1.5em; color: #333; font-weight: bold;">10</span></h1>
					 					</center>
									</div>
									<div class="col-lg-4">
										<center><img src="img/10.png" class="img-responsive" /></center>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		


		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
	</body>
</html>